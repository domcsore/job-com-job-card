import React from 'react';
import ReactDom from 'react-dom';

import CardDeck from './components/CardDeck';

const App = (props) => {
    return (
        <CardDeck jobApiUrl={'/jobs.json'} newAge={100} />
    )
}

ReactDom.render(<App />, document.getElementById("app"));