import React from 'react';

import './Card.scss';

const Card = (props) => {
    return (
        <div className={'card'}>
            <div className={'card-header'}>
                {props.job.logo ? <img src={props.job.logo} className={'card-logo'} alt={`${props.job.company} logo`}/> : <p className={'card-logo initials'}>{props.job.companyInitials}</p>}
                {props.job.daysPassed < props.newAge ? <p className={'card-new'}>New</p> : ''}
            </div>
            <h2 className={'card-title'}>{props.job.title}</h2>
            <p>{props.job.company} - <span className={'card-location'}>{props.job.city}</span></p>
            <p>{props.job.salaryFormatted} - {props.job.salaryToFormatted}</p>
            <div className={'card-footer'}>
                <p>Posted {props.job.daysPassed} days ago</p>
                <OpenLink link={'#'} />
            </div>
        </div>
    )
}

const OpenLink = (props) => {
    return (
        <a href={props.link}>
            <svg width="32" height="30" viewBox="0 0 32 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="6" y="6" width="20" height="18">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M23.4666 22H8.53324V8H15.9999V6H8.53324C7.34924 6 6.3999 6.9 6.3999 8V22C6.3999 23.1 7.34924 24 8.53324 24H23.4666C24.6399 24 25.5999 23.1 25.5999 22V15H23.4666V22ZM18.1332 6V8H21.9626L11.4772 17.83L12.9812 19.24L23.4666 9.41V13H25.5999V6H18.1332Z" fill="white"/>
                </mask>
                <g mask="url(#mask0)">
                    <rect x="2.1333" y="2" width="27.7333" height="26" fill="#4D4D4D"/>
                </g>
            </svg>
        </a>
    )
}

export default Card;