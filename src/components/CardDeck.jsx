import React from 'react';
import 'whatwg-fetch';

import * as Job from '../Job';
import Card from './Card';

import './CardDeck.scss';

const CardDeck = (props) => {
    const [jobs, setJobs] = React.useState([]);

    React.useEffect(() => {
        Job.getJobsFromApi(props.jobApiUrl).then(jobs => {setJobs(jobs)}).catch(error => {/*handle api error*/})
    }, [])

    return (
        <div className={'card-deck'}>
            {jobs.map(job => <Card job={job} newAge={props.newAge}/>)}
        </div>
    )
}

export default CardDeck;