export function Job(company, logo, title, location, salary, salaryTo, salaryCurrency, liveDate) {
    this.company = company;
    this.logo = logo;
    this.title = title;
    this.location = location;
    this.salary = salary;
    this.salaryTo = salaryTo;
    this.salaryCurrency = salaryCurrency;
    this.liveDate = Date.parse(liveDate);
    Object.defineProperty(this, 'daysPassed', {
        get: () => msToDays(Date.now() - this.liveDate)
    });
    Object.defineProperty(this, 'salaryFormatted', {
        get: () => currencyNumberFormat(this.salary, this.salaryCurrency)
    });
    Object.defineProperty(this, 'salaryToFormatted', {
        get: () => currencyNumberFormat(this.salaryTo, this.salaryCurrency)
    });
    Object.defineProperty(this, 'companyInitials', {
        get: () => getInitials(this.company)
    });
    Object.defineProperty(this, 'city', {
        get: () => this.location.split(',')[0]
    });
}

export const getInitials = (name) => {
    let initials = name.match(/\b\w/g);
    if (!initials) return '';
    return initials.join('');
}

export const currencyNumberFormat = (number, currencyCode) => {
    let formattedNumber = new Intl.NumberFormat().format(number);
    if (currencies[currencyCode]) {
        return currencies[currencyCode] + formattedNumber;
    } else {
        return formattedNumber;
    }
}

const currencies = {
    USD: '$',
    GBP: '£',
    EUR: '€',
}

export const msToDays = (ms) => {
    return Math.round(ms / 1000 / 60 / 60 / 24)
}

export const jobsFromJson = (json) => {
    if (!json.results) throw Error('invalid json input - expected results property');

    return json.results.map(job => new Job(
        job.company.name,
        job.company.logo,
        job.title,
        job.location,
        job.salary,
        job.salaryTo,
        job.salaryCurrencyCode,
        job.liveDate
    ));
}

export const getJobsFromApi = (apiUrl) => {
    return new Promise((resolve, reject) => {
        window.fetch(apiUrl, {
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
            }
        }).then(response => response.json()).then(json => {
            try {
                resolve(jobsFromJson(json));
            } catch (e) {
                reject(e);
            }
        }).catch(error => reject(error));
    });
}