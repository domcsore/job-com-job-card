import * as Job from '../src/Job';

// Initials
test('Initials should be T', () => {
    expect(Job.getInitials('Test')).toBe('T');
})

test('Initials should be TI', () => {
    expect(Job.getInitials('Test Initials')).toBe('TI');
})

test('Initials should be TIF', () => {
    expect(Job.getInitials('Test Initials Function')).toBe('TIF');
})

test('Initials should be empty', () => {
    expect(Job.getInitials('')).toBe('');
})

// Currency format
test('Formatted currency should be $1,200', () => {
    expect(Job.currencyNumberFormat(1200, 'USD')).toBe('$1,200');
})

test('Formatted currency should be £1,200', () => {
    expect(Job.currencyNumberFormat(1200, 'GBP')).toBe('£1,200');
})

test('Formatted currency should be $1,200,000', () => {
    expect(Job.currencyNumberFormat(1200000, 'USD')).toBe('$1,200,000');
})

test('Formatted currency should be 1,200,000', () => {
    expect(Job.currencyNumberFormat(1200000, 'YEN')).toBe('1,200,000');
})

// Milliseconds to days
test('Days should be 1', () => {
    expect(Job.msToDays(86400000)).toBe(1);
})

test('Days should be 0', () => {
    expect(Job.msToDays(8640000)).toBe(0);
})

test('Days should be 5', () => {
    expect(Job.msToDays(432000000)).toBe(5);
})

// Jobs from JSON
test('Jobs should be populated', () => {
    const jobsJson = {
        "results": [{
            "company": {
                "name": "Nest",
                "logo": null
            },
            "title": "Java Developer & Systems Administrator",
            "description": "Java Developer",
            "location": "New york",
            "salaryCurrencyCode": "USD",
            "salary": 100000,
            "salaryTo": 120000,
            "employmentType": "PERMANENT",
            "salaryType": "ANNUALLY",
            "liveDate": "2020-08-31",
            "expiryDate": "2099-09-30",
            "id": 1,
            "numViewed": 43,
            "jobSector": "IAC-01-06-05"
        }],
        total: 1
    }

    const expected = new Job.Job(
        "Nest",
        null,
        "Java Developer & Systems Administrator",
        "New york",
        100000,
        120000,
        "USD",
        "2020-08-31"
    )

    expect(Job.jobsFromJson(jobsJson)[0]).toEqual(expected);
})

test('Should throw exception', () => {
    const jobsJson = {
        "company": {
            "name": "Nest",
            "logo": null
        },
        "title": "Java Developer & Systems Administrator",
        "description": "Java Developer",
        "location": "New york",
        "salaryCurrencyCode": "USD",
        "salary": 100000,
        "salaryTo": 120000,
        "employmentType": "PERMANENT",
        "salaryType": "ANNUALLY",
        "liveDate": "2020-08-31",
        "expiryDate": "2099-09-30",
        "id": 1,
        "numViewed": 43,
        "jobSector": "IAC-01-06-05"
    }
    
    expect(() => Job.jobsFromJson(jobsJson)).toThrow();
})