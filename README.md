# Job.com Job Card Component
## To install
1. Ensure `node` is installed.
2. From the root directory, run `npm install` to install dependencies.
3. From the root directory, run `npm run test` to ensure all tests are passing.
4. From the root directory, run `npm run serve` to run a development server.
5. Navigate to `http://localhost:9000` in your browser.